## Diff # iki dosya arasındaki farkları bulur.

* `diff dosya1 dosya2`  # dosya1 ile dosya2 yi karşılaştır farklı satırları listeler

* `diff -rq dizin1 dizin2`  # dizin1 ile dizin2 içindeki dosyaları alt dizinleri de kapsayarak aralarındaki farkı bul
* `diff -rq dizin1 dizin2 | grep differ |nl`  # farklı olanları sıra nosu ile göster
* `diff -rq dizin1 dizin2 | grep Only |nl`  # ayni olanları sıranosu ile göster.
