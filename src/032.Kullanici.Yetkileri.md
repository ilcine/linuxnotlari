## Kullanıcı işlemleri  

* `sudo adduser abc` # abc adlı kullanıcı yaratılır, `/etc/password`, `/etc/shadow`, `/etc/groups`
 ta kayıtlar oluşur. (root yetkisi gerekir) adduser ile kullanıcı yaratıldığında kullanıcının 
 dizininde de dosyalar oluşur . ile başlayan dosyalar kullanıcıya atanan config(.bashrc, .bash_history vb)
 dosyalarıdır bu dosyada yapılacak değişikliklerden sadece o kullanıcıyı etkiler; `/etc/groups` ta `abc:x:1000:` kaydı oluşur; kullanıcıya `/home/abc` dizini açılır 

* `passwd abc` # abc kullanıcısının şifresi değiştirilir.

* ~/.bash_history  # Klavyeden girilen komutların tutulduğu dosya; `~/.bashrc` ye `export HISTFILESIZE=1000` yazılırsa 1000 adet komu o kullanıcı için tutar,
 /etc/profile içine yazılırsa tüm kullanıcılar için tutar. Unlimit için `export HISTFILESIZE=` ve `export HISTSIZE=`

* `~/.bash_logout`  # kullanıcı exit yapınca yapılacak işlemler
* `~/.bashrc` # kullanıcı login olunca yapılacak işler.
* `~/.profile` # kullanıcı ya atanan path(yol) vb ifadeler. 

* `deluser abc` #  abc adlı kullanıcıyı siler :x:

* `chfn abc` # abc kullanıcısının özellikleri değiştirilir
* `sudo su -` # SuperUser moda geçilir. (prompt `#` halini alır, normal pormpt `$` dır)
* `exit` # supermod dan çıkılır,(birçok uygulamadan çıkıştıda kullanılır)
* `/etc/sudoers` # Dosyası içine `abc  ALL=(ALL:ALL) ALL` verilirse abc kullanıcısı superuser olur. Duruma göre daha az yetkilerde verilir.
* `/etc/profile` # Tüm kullanıcıla atanacak path vb ifadeler tanımlanır.
* `/etc/aliases` # Komutlara kısayol veya kısa komut tanımı yapılır. 
 (ör: `alias l='ls -la'` veya `alias dir='ls -la'`  gibi ifadeler "/etc/aliases" dosyasına yazılırsa sürekli, komut setine yazılırsa login olunduğu sürece
 l veya dir diyerek `ls -la` komutu çalıştırılmış olur.) 
 /etc/aliases dosyasına yazılanların aktif olabilmesi için `newaliases` komutunun çalıştırılması gerekir. 

> * not1: `~/` tilde ifadesi kullanıcının home dizinini ifade eder ör:/home/abc, `~/.bashrc` ile `/home/abc/.bashrc` ayni şeydir.
> * not2: profile ye `export HISTSIZE=10000` yazılırsa klavyeden girilmiş son 10000 komut `.bash_histroy` dosyasında saklanır (güvenlik için önemli), komutun aktif olması için logout ve login gerekir veya `source .profile` komutu ile HISTSIZE aktif hale getirilir.
> * not3: komut yazılırken bir boşluk verilirse ~.bash_history~ e birşey yazılmaz.
> * not4: profile ye yazılan `umask 022` ile yaratılacak dosya ve dizinlerin default yetkileri (644) tanımlanır. `(because 0666 & ~022 = 0644; i.e., rw-r--r--)` , erişim yetkilerinin neler olduğu `acl` (access control list) te tanımlanmıştır. 


