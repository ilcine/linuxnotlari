# CRONTAB (zamanlanmış görevler)

* `sudo update-alternatives --config editor` # kullanılacak editörü seç 
* `crontab -l` # yazılanlar listelenir.
* `crontab -e` # ile editleme yap

```
.---------------- dakika (0 - 59)
|  .------------- saat (0 - 23)
|  |  .---------- Ayın Günleri (1 - 31)
|  |  |  .------- Ay (1 - 12)
|  |  |  |  .---- Haftanın Günleri (0 - 6) (Pazar=0)
|  |  |  |  |
*  *  *  *  *  Çalıştırılacak komut
```

* (*) ile işin her gün,her ay, her saat vb çalışması istenir 
Örnekler
* `* 17 * * * sudo /home/emr/run.sh` : saat 17 yi gösterdiğinde run.sh script kodu çalışsın. `
* `*/5 * * * * sudo /home/emr/run.sh` : her 5 dk bir run.sh script kodu çalışsın. (/) ile kademeler belirlenir.
* `* */5 * * * sudo /home/emr/run.sh` : her 5 saate bir run.sh çalışsın.
* `* 1 1-15 * * sudo /home/emr/run.sh` : Her ayın 1 inde ve 15 inde, saat 1 de run.sh çalışsın. (-) ile zaman aralığı belirlenir.
* `0 0 1 1 * sudo /home/emr/run.sh` : Yeni yılın ilk günü saat 0 0 da çalışsın. 
* `0 9-17 * * * sudo /home/emr/run.sh` : Her gün saat 9 dan 17 ye kadar çalışsın sonra dursun. 
* `0 12 * 2 1,5 sudo /home/emr/run.sh` : Şubat ayının her pazartesi ve cuma günü saat 12 de çalışsın. (,) ile birden fazla değer girilir.
* `0 12,18 * * * sudo bash /home/emmr.sh` : Her gün saat 12 ve 18 de çalışşın.

## Standart çıktılar

> `0 STDIN, 1 STDOUT, 2 STDERR` # içeri, dışarı ve error dur

> `> /dev/null 2>&1`  çıktı dev/null a gidecek hata varsa standart çıktıya yönlenecek.

> `> /dev/null` standart çıktı yok. crontab çıktısı e-mail olarak kullanıcıya gider. 

> ex1: `*/5 * * * * ping -c 1 google.com > /dev/null` sistem her 5 dk bir google a bir adet ping çeker, standart çıktı dev/null dur, tanımlanmış adrese mail atabilir.

> ex2: `* 1 * * * ping -c 1 google.com > /tmp/ping.log` # saat 1 de google a ping bir adet çeksin ve log a yazsın.

> crontab a default mail tanımı için `MAILFROM=sender@mydomain.com MAILTO=recipient@mydomain.com` yapalabileceği gibi direkt 
yönlendirme de yapılır. `* * * * * run.sh 2>&1 | mail -s "Konu" -S from=sender@mydomain.com recipient@mydomain.com`



